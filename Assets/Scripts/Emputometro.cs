﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Emputometro : MonoBehaviour
{
    public float LoseMax;
    public static float Losing;
    public Image Tacometro;

    // Start is called before the first frame update
    void Start()
    {
        Tacometro = transform.Find("Emputometro").GetComponent<Image>();
        Losing = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Perdidas();
    }

    void Perdidas() 
    {
        if (Tacometro.fillAmount < 0.5f) {


            Tacometro.fillAmount = (Losing / (2*LoseMax));

        }else if(Losing >=LoseMax) {

            PJController.ingame = false;
            SceneManager.LoadScene("Lose");
        }
    }
}
