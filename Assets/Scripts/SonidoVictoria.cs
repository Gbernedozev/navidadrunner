﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoVictoria : MonoBehaviour
{
    public AudioClip audio;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayAudio() {

        FindObjectOfType<SoundManager>().Pausar("Nivel1");
        audioSource.PlayOneShot(audio);
    }
}
