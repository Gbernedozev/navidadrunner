﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constructor : MonoBehaviour
{
    public GameObject[] SpawnerBasuras;
    public GameObject[] Prefabs;
    private Transform ActualSpawner;
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Builder", 0, 0.5f);

    }

    // Update is called once per frame
    void Update()
    {

        
    }
    void Builder() 
    {
        GameObject obj = Instantiate(Prefabs[Random.Range(0, Prefabs.Length)]);
        ActualSpawner = SpawnerBasuras[Random.Range(0, SpawnerBasuras.Length)].transform;
        obj.transform.position = new Vector2(ActualSpawner.transform.position.x, ActualSpawner.transform.position.y);
    }
}
