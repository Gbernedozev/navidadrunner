﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJAnimController : MonoBehaviour
{
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Win.isWin) 
        {
            anim.updateMode = AnimatorUpdateMode.UnscaledTime;
            anim.SetBool("Ganar", true);
        }
        if (!Win.isWin) 
        {

            
            anim.SetBool("Ganar", false);
        }
        
    }
    
}
