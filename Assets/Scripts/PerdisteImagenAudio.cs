﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerdisteImagenAudio : MonoBehaviour
{
    public AudioClip audio;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void PlayAudio()
    {
        audioSource.PlayOneShot(audio);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
