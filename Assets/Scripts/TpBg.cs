﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TpBg : MonoBehaviour
{
    public GameObject Last;
    public GameObject[] BG;
    public GameObject Referencia;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Trasladar",0,1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Trasladar() 
    {
        for(int i=0; i < 4; i++)
        {
            if (BG[i].transform.position.y <=Referencia.transform.position.y) 
            {
                BG[i].transform.position = new Vector3(Last.transform.position.x,
                    Last.transform.position.y + 10.2f,
                    Last.transform.position.z);

                    Last= BG[i].gameObject;
            }
        }
    }
    /*
    private void OnTriggerEnter2D(Collider2D other) {

        other.transform.localPosition = new Vector3(Last.transform.position.x,
          Last.transform.position.y + 10f,
          Last.transform.position.z);
        Last = other.gameObject;

    }
    */
}
