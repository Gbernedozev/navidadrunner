﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarSound : MonoBehaviour
{
    public AudioClip audio;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void PlayAudio() {

        audioSource.PlayOneShot(audio);
    }
}
