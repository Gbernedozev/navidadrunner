﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public static bool isWin;
    public GameObject PanelGanar;

    // Start is called before the first frame update
    void Start()
    {
        isWin = false;

        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isWin) {
            Activador();
            
            PJController.ingame = false;
        }
        if (!isWin) {
            Time.timeScale = 1;
            PJController.ingame = true;
        }
    }

    void Activador() 
    {
        
        PanelGanar.SetActive(true);
        Time.timeScale = 0;
    }

    

}
