﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoItems : MonoBehaviour
{
    private float speed=7;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
        if (Win.isWin) { this.gameObject.SetActive(false); }
    }
    void Movimiento() 
    {
        rb.velocity = new Vector2(rb.velocity.x, -1 * speed);
    }


}
