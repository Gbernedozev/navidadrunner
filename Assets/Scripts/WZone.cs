﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WZone : MonoBehaviour
{
    public Animator anim;
    public GameObject Policia;
    public Policia_Model molesto;
    private float timer;
    public GameObject particlePrefab;
    // Start is called before the first frame update
    void Start()
    {
        anim=Policia.GetComponent<Animator>();
        molesto=Policia.GetComponent<Policia_Model>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((molesto.isAngry == true) && (timer <= Time.time)) {
            molesto.isAngry = false;
            anim.SetBool("Penal", molesto.isAngry);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) {
       
        if (collision.gameObject.tag == "Basura") {
            
            molesto.isAngry = true;
            timer = Time.time + 0.35f;
            Emputometro.Losing += 1;
            anim.SetBool("Penal", molesto.isAngry);
            
            FindObjectOfType<SoundManager>().Play("Pitillo");

            /*
            GameObject obj=Instantiate(particlePrefab);
            Vector3 pos= collision.gameObject.transform.position;
            pos.y += 0.5f;

            obj.transform.position = pos;

            */
             
            Destroy(collision.gameObject,2);

            
        }
       

    }

    
}
