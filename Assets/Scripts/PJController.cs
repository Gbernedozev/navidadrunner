﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform buttonA;
    public Transform buttonS;
    public Transform buttonD;

    public GameObject ParticlesPrefab1;
    public GameObject ParticlesPrefab2;

    public int index = 0;   
    

    public static bool ingame;
    

    void Start()
    {
        ingame = true;

        FindObjectOfType<SoundManager>().Play("Nivel1");

       
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();

        

    }

    void Movimiento() 
    {
        

        if (ingame) 
        {

            if (Input.GetKeyDown(KeyCode.LeftArrow)) {

                if (index>-1) {
                    index--;
                    FindObjectOfType<SoundManager>().Play("CambioCarril");
                }
                
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) {

                if (index < 1) {
                    index++;
                    FindObjectOfType<SoundManager>().Play("CambioCarril");
                }
                
            }

            

            /*
            if (Input.GetKeyDown(KeyCode.A)) {
                
                gameObject.transform.position = new Vector2(buttonA.transform.position.x, gameObject.transform.position.y);
            }
            if (Input.GetKeyDown(KeyCode.S)) {
                gameObject.transform.position = new Vector2(buttonS.transform.position.x, gameObject.transform.position.y);
            }
            if (Input.GetKeyDown(KeyCode.D)) {
                gameObject.transform.position = new Vector2(buttonD.transform.position.x, gameObject.transform.position.y);
            }
            */

            switch (index) 
                {
                case -1:
                    gameObject.transform.position = new Vector2(buttonA.transform.position.x, gameObject.transform.position.y);
                    break;
                case 0:
                    gameObject.transform.position = new Vector2(buttonS.transform.position.x, gameObject.transform.position.y);
                    break;
                case 1:
                    gameObject.transform.position = new Vector2(buttonD.transform.position.x, gameObject.transform.position.y);
                    break;
            }

        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.gameObject.tag == "Basura") {
            Score.puntaje += 100;
            BarraBasura.Collect += 1;

            GameObject obj = Instantiate(ParticlesPrefab1);
            Vector3 pos = collision.gameObject.transform.position;

            FindObjectOfType<SoundManager>().Play("Recolectar");
            obj.transform.position = pos;

            Destroy(collision.gameObject);

        }
        if (collision.gameObject.tag == "Animal") 
        {
            /*
            molesto.isAngry = true;
            timer = Time.time + 0.35f;

            anim.SetBool("Penal", molesto.isAngry);
            */
            Emputometro.Losing += 1;
            GameObject obj = Instantiate(ParticlesPrefab2);
            Vector3 pos = collision.gameObject.transform.position;
            obj.transform.position = pos;


            FindObjectOfType<SoundManager>().Play("Pitillo");

            Destroy(collision.gameObject);
        }

    }
}
