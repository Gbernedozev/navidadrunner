﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonS : MonoBehaviour
{
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) {
            anim.SetBool("Open", true);

        } else if (Input.GetKeyUp(KeyCode.S)) { anim.SetBool("Open", false); }

    }


    private void OnTriggerStay2D(Collider2D other) {

        if (Input.GetKeyDown(KeyCode.S)) {
            if (other.gameObject.tag == "Plastico") {
                Score.puntaje += 100;
                BarraBasura.Collect += 1;
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "Papel") {
                Score.puntaje += 100;
                BarraBasura.Collect += 1;
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "Metal") {
                Score.puntaje += 100;
                BarraBasura.Collect += 1;
                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "Basura") {
                Score.puntaje += 100;
                BarraBasura.Collect += 1;
                Destroy(other.gameObject);
            }
        }
    }
}
