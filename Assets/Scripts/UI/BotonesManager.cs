﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonesManager : MonoBehaviour
{
    public Boton[] botones;
    public int Posicion = 0;

    // Start is called before the first frame update
    void Start() {
        botones[Posicion].Seleccionado = true;
        Win.isWin = false;
        PJController.ingame = false;
        //ocultar cursor del mouse:
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
        if (PJController.ingame == false && botones.Length >1) {

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
                botones[Posicion].Seleccionado = false;
                FindObjectOfType<SoundManager>().Play("Seleccion");
                Posicion--;

                if (Posicion < 0) {
                    Posicion = botones.Length - 1;
                    FindObjectOfType<SoundManager>().Play("Seleccion");
                    botones[Posicion].Seleccionado = true;
                    return;
                }


                botones[Posicion].Seleccionado = true;
            }

            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
                botones[Posicion].Seleccionado = false;
                FindObjectOfType<SoundManager>().Play("Seleccion");
                Posicion++;


                if (Posicion > botones.Length - 1) {
                    Posicion = 0;
                    FindObjectOfType<SoundManager>().Play("Seleccion");
                    botones[Posicion].Seleccionado = true;
                    return;
                }

                botones[Posicion].Seleccionado = true;
            }
        }

    }
}
