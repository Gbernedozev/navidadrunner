﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Boton : MonoBehaviour
{
    public string Escena;

    public bool Seleccionado = false;

    private bool IsSelected = false;
    private float Delay = 0.5f;
    private float Tiempo;

    Animator anim;

    // Start is called before the first frame update
    void Start() {
        Tiempo = Delay;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {

        if (Seleccionado) {
            //BotonImage.color = colores[1];
            anim.SetBool("isSelected", true);
        } else {
            //BotonImage.color = colores[0];
            anim.SetBool("isSelected", false);
        }

        if (Input.GetKeyDown(KeyCode.Return) && Seleccionado) {
            FindObjectOfType<SoundManager>().Play("Press");
            IsSelected = true;

            anim.SetBool("isPress", true);
            if (Tiempo <= 0) { SceneManager.LoadScene(Escena, LoadSceneMode.Single); }

        }
        if (IsSelected) {
            Tiempo -= Time.unscaledDeltaTime; //Se usa el unscaleDeltaTime para que Time.Scale no afecte el proceso
            if (Tiempo <= 0) {
                Tiempo = Delay;
                IsSelected = false;

                if (Escena == "Exit") {
                    Application.Quit();
                    return;
                }

                SceneManager.LoadScene(Escena, LoadSceneMode.Single);
            }

        }


    }
}
