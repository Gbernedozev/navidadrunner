﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BarraBasura : MonoBehaviour
{
    public float WinMax;
    public static float Collect;
    public Image barImage;
    // Start is called before the first frame update
    void Start()
    {
        barImage= transform.Find("Bar").GetComponent<Image>();
        Collect = 0;

    }

    // Update is called once per frame
    void Update()
    {
        Collecting();
       
        
    }
    void Collecting() 
    {
        if (barImage.fillAmount <=1) {

            
            barImage.fillAmount = Collect/WinMax;

        } 
        if (Collect >=WinMax) {

            PJController.ingame = false;
            Win.isWin = true;
            //SceneManager.LoadScene("Win");
        }
        
    }
}
